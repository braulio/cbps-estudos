#!/usr/bin/env sh


aluno1=joão
aluno2=maria
aluno3=antônio
aluno4='luis_carlos'

joao=8.5
maria=9.0
antonio=6.5
luis_carlos=9.5

clear
echo ----------
echo $aluno3
echo $aluno1
echo $aluno4
echo $aluno2
echo ----------
echo
read -p 'Digite o nome de um(a) aluno(a): ' nome
echo
echo ========
echo "A nota é ${${nome}}"
echo ========
